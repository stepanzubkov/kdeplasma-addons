# translation of plasma_applet_calculator.po to Thai
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Thanomsub Noppaburana <donga.nb@gmail.com>, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_calculator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-08 01:58+0000\n"
"PO-Revision-Date: 2010-03-07 23:29+0700\n"
"Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: package/contents/ui/calculator.qml:227
#, kde-format
msgctxt ""
"Abbreviation for result (undefined) of division by zero, max. six to nine "
"characters."
msgid "undef"
msgstr ""

#: package/contents/ui/calculator.qml:336
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr ""

#: package/contents/ui/calculator.qml:359
#, fuzzy, kde-format
#| msgctxt "The C button of the calculator"
#| msgid "C"
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:372
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr ""

#: package/contents/ui/calculator.qml:385
#, fuzzy, kde-format
#| msgctxt "The × button of the calculator"
#| msgid "×"
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:397
#, fuzzy, kde-format
#| msgctxt "The AC button of the calculator"
#| msgid "AC"
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:449
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr ""

#: package/contents/ui/calculator.qml:501
#, fuzzy, kde-format
#| msgctxt "The + button of the calculator"
#| msgid "+"
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:552
#, fuzzy, kde-format
#| msgctxt "The = button of the calculator"
#| msgid "="
msgctxt "Text of the equals button"
msgid "="
msgstr "="

#~ msgctxt "The − button of the calculator"
#~ msgid "−"
#~ msgstr "−"

#~ msgid "ERROR"
#~ msgstr "ผิดพลาด"

#~ msgid "ERROR: DIV BY 0"
#~ msgstr "ผิดพลาด: ถูกหารด้วย O"

#~ msgid "Copy"
#~ msgstr "คัดลอก"

#~ msgid "Paste"
#~ msgstr "วาง"

#~ msgctxt "The ∕ button of the calculator"
#~ msgid "∕"
#~ msgstr "∕"
