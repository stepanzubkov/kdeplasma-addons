# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2008, 2009, 2015, 2018, 2019.
# Jan Madsen <jan-portugal@opensuse.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-02 02:09+0000\n"
"PO-Revision-Date: 2019-11-22 20:25+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Udseende"

#: package/contents/ui/configAppearance.qml:32
#, kde-format
msgid "%1pt"
msgstr ""

#: package/contents/ui/configAppearance.qml:38
#, fuzzy, kde-format
#| msgid "Use custom font size:"
msgid "Text font size:"
msgstr "Brug selvvalgt skriftstørrelse:"

#: package/contents/ui/configAppearance.qml:43
#, kde-format
msgid "Background color"
msgstr ""

#: package/contents/ui/configAppearance.qml:74
#, kde-format
msgid "A white sticky note"
msgstr "En hvis post-it"

#: package/contents/ui/configAppearance.qml:75
#, kde-format
msgid "A black sticky note"
msgstr "En sort post-it"

#: package/contents/ui/configAppearance.qml:76
#, kde-format
msgid "A red sticky note"
msgstr "En rød post-it"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "An orange sticky note"
msgstr "En orange post-it"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A yellow sticky note"
msgstr "En gul post-it"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A green sticky note"
msgstr "En grøn post-it"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "A blue sticky note"
msgstr "En blå post-it"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A pink sticky note"
msgstr "En lyserød post-it"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A translucent sticky note"
msgstr "En gennemsigtig post-it"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "En gennemsigtig post-it med lys tekst"

#: package/contents/ui/main.qml:265
#, kde-format
msgid "Undo"
msgstr "Fortryd"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Redo"
msgstr "Gendan"

#: package/contents/ui/main.qml:283
#, kde-format
msgid "Cut"
msgstr "Klip"

#: package/contents/ui/main.qml:291
#, kde-format
msgid "Copy"
msgstr "Kopiér"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Paste"
msgstr "Indsæt"

#: package/contents/ui/main.qml:305
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgid "Paste with Full Formatting"
msgstr "Indsæt uden formatering"

#: package/contents/ui/main.qml:312
#, fuzzy, kde-format
#| msgid "Formatting"
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "Formatering"

#: package/contents/ui/main.qml:327
#, kde-format
msgid "Delete"
msgstr "Slet"

#: package/contents/ui/main.qml:334
#, kde-format
msgid "Clear"
msgstr "Ryd"

#: package/contents/ui/main.qml:344
#, kde-format
msgid "Select All"
msgstr "Markér alt"

#: package/contents/ui/main.qml:472
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Fed"

#: package/contents/ui/main.qml:484
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Kursiv"

#: package/contents/ui/main.qml:496
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Understreget"

#: package/contents/ui/main.qml:508
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Gennemstreget"

#: package/contents/ui/main.qml:582
#, kde-format
msgid "Discard this note?"
msgstr ""

#: package/contents/ui/main.qml:583
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr ""

#: package/contents/ui/main.qml:597
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Hvid"

#: package/contents/ui/main.qml:601
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Sort"

#: package/contents/ui/main.qml:605
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Rød"

#: package/contents/ui/main.qml:609
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Orange"

#: package/contents/ui/main.qml:613
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Gul"

#: package/contents/ui/main.qml:617
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Grøn"

#: package/contents/ui/main.qml:621
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Blå"

#: package/contents/ui/main.qml:625
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Pink"

#: package/contents/ui/main.qml:629
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "Gennemsigtig"

#: package/contents/ui/main.qml:633
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "Gennemsigtig lys"

#~ msgid "Toggle text format options"
#~ msgstr "Slå tekstformat-indstillinger til/fra"

#~ msgid "Notes Settings..."
#~ msgstr "Indstilling af noter..."

#, fuzzy
#~| msgid "Justify center"
#~ msgid "Align center"
#~ msgstr "Centreret"

#, fuzzy
#~| msgid "Justify"
#~ msgid "Justified"
#~ msgstr "Justeret"

#~ msgid "Font"
#~ msgstr "Skrifttype"

#~ msgid "Style:"
#~ msgstr "Stil:"

#~ msgid "&Bold"
#~ msgstr "&Fed"

#~ msgid "&Italic"
#~ msgstr "&Kursiv"

#~ msgid "Size:"
#~ msgstr "Størrelse:"

#~ msgid "Scale font size by:"
#~ msgstr "Skalér skrifttstørrelse efter:"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Color:"
#~ msgstr "Farve:"

#~ msgid "Use theme color"
#~ msgstr "Brug temaets farve"

#~ msgid "Use custom color:"
#~ msgstr "Brug selvvalgt farve:"

#~ msgid "Active line highlight color:"
#~ msgstr "Fremhævningsfarve til aktiv linje:"

#~ msgid "Use no color"
#~ msgstr "Brug ikke en farve"

#~ msgid "Theme"
#~ msgstr "Tema"

#~ msgid "Notes color:"
#~ msgstr "Noters farve:"

#~ msgid "Spell Check"
#~ msgstr "Stavekontrol"

#~ msgid "Enable spell check:"
#~ msgstr "Aktivér stavekontrol"

#~ msgid "Notes Color"
#~ msgstr "Noters farve"

#~ msgid "General"
#~ msgstr "Generelt"

#~ msgid "Unable to open file"
#~ msgstr "Kunne ikke åbne fil"

#~ msgid "Welcome to the Notes Plasmoid! Type your notes here..."
#~ msgstr "Velkommen til Notes-plasmoiden! Indtast dine noter her..."

#~ msgid "Text Color"
#~ msgstr "Tekstfarve"

#, fuzzy
#~| msgid "Font:"
#~ msgid "Font style:"
#~ msgstr "Skrifttype:"

#, fuzzy
#~| msgid "Font:"
#~ msgid "Font size:"
#~ msgstr "Skrifttype:"

#~ msgid "Check spelling"
#~ msgstr "Stavekontrol"

#~ msgid "Notes Configuration"
#~ msgstr "Konfiguration af Notes"

#~ msgid "Select Font..."
#~ msgstr "Vælg skrifttype..."
