# Translation of plasma_applet_org.kde.plasma.binaryclock.pot to esperanto.
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the kdeplasma-addons package.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_binaryclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2023-01-08 21:45+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:17
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Aspekto"

#: package/contents/ui/configGeneral.qml:35
#, kde-format
msgid "Display:"
msgstr "Bildigo:"

#: package/contents/ui/configGeneral.qml:36
#: package/contents/ui/configGeneral.qml:90
#, kde-format
msgctxt "@option:check"
msgid "Grid"
msgstr "Krado"

#: package/contents/ui/configGeneral.qml:41
#: package/contents/ui/configGeneral.qml:77
#, kde-format
msgctxt "@option:check"
msgid "Inactive LEDs"
msgstr "Neaktivaj LEDoj"

#: package/contents/ui/configGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Seconds"
msgstr "Sekundoj"

#: package/contents/ui/configGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "In BCD format (decimal)"
msgstr "En BCD-formato (duum-dekuma)"

#: package/contents/ui/configGeneral.qml:60
#, kde-format
msgid "Use custom color for:"
msgstr "Uzi ropran koloron por:"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Active LEDs"
msgstr "Aktivaj LEDoj"
