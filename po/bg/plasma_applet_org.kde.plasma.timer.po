# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-29 01:59+0000\n"
"PO-Revision-Date: 2022-12-02 00:11+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Външен вид"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Predefined Timers"
msgstr "Предварително зададени таймери"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr "Разширени"

#: package/contents/ui/CompactRepresentation.qml:134
#, kde-format
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "Паузиране на таймера"

#: package/contents/ui/CompactRepresentation.qml:134
#, kde-format
msgctxt "@action:button"
msgid "Start Timer"
msgstr "Стартиране на таймера"

#: package/contents/ui/CompactRepresentation.qml:191
#: package/contents/ui/CompactRepresentation.qml:208
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] "%1s"
msgstr[1] "%1s"

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr "След завършване на таймера:"

#: package/contents/ui/configAdvanced.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Execute command:"
msgstr "Изпълнение на команда:"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr "Дисплей:"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show title:"
msgstr "Показване на заглавие:"

#: package/contents/ui/configAppearance.qml:53
#, kde-format
msgctxt "@option:check"
msgid "Show remaining time"
msgstr "Показване на оставащо време"

#: package/contents/ui/configAppearance.qml:59
#, kde-format
msgctxt "@option:check"
msgid "Show seconds"
msgstr "Показване на секунди"

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Show timer toggle"
msgstr "Показване на превключвател:"

#: package/contents/ui/configAppearance.qml:69
#, kde-format
msgctxt "@option:check"
msgid "Show progress bar"
msgstr "Показване на лентата за прогрес"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr "Известия:"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr "Показване на текста на известието:"

#: package/contents/ui/configTimes.qml:77
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""
"Ако добавите предварително зададени таймери тук, те ще се появят в "
"контекстното меню на Plasma приставките."

#: package/contents/ui/configTimes.qml:84
#, kde-format
msgid "Add"
msgstr "Добавяне"

#: package/contents/ui/configTimes.qml:123
#, kde-format
msgid "Scroll over digits to change time"
msgstr "Превъртете над цифрите, за да промените времето"

#: package/contents/ui/configTimes.qml:129
#, kde-format
msgid "Apply"
msgstr "Прилагане"

#: package/contents/ui/configTimes.qml:137
#, kde-format
msgid "Cancel"
msgstr "Отказ"

#: package/contents/ui/configTimes.qml:146
#, kde-format
msgid "Edit"
msgstr "Редактиране"

#: package/contents/ui/configTimes.qml:155
#, kde-format
msgid "Delete"
msgstr "Изтриване"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "%1 is running"
msgstr "%1 се изпълнява"

#: package/contents/ui/main.qml:69
#, kde-format
msgid "%1 not running"
msgstr "%1 не се изпълнява"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] "Оставащо време: %1 секунда"
msgstr[1] "Оставащо време: %1 секунди"

#: package/contents/ui/main.qml:72
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""
"Използване на колелото на мишката, за да промените цифрите или да изберете "
"от предварително зададени таймери в контекстното меню"

#: package/contents/ui/main.qml:89
#, kde-format
msgid "Timer"
msgstr "Таймер"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Timer finished"
msgstr "Таймерът завърши"

#: package/contents/ui/main.qml:130
#, kde-format
msgctxt "@action"
msgid "&Start"
msgstr "&Старт"

#: package/contents/ui/main.qml:135
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr "Ст&оп"

#: package/contents/ui/main.qml:140
#, kde-format
msgctxt "@action"
msgid "&Reset"
msgstr "&Нулиране"
